package com.example.a0807baseui_linear

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val NAME = "NAME"
    private val GROUP = "GROUP"
    private val MESSAGE = "MESSAGE"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnSend.setOnClickListener { updateTextFields() }
    }

    private fun updateTextFields() {
        txtName.text = spin_name.selectedItem.toString()
        txtGroup.text = spin_group.selectedItem.toString()
        txtMessage.text = etMessage.text
        etMessage.text.clear()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putString(NAME, txtName.text.toString())
            putString(GROUP, txtGroup.text.toString())
            putString(MESSAGE, txtMessage.text.toString())
        }
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        txtName.text = savedInstanceState.getString(NAME)
        txtGroup.text = savedInstanceState.getString(GROUP)
        txtMessage.text = savedInstanceState.getString(MESSAGE)
        super.onRestoreInstanceState(savedInstanceState)
    }
}