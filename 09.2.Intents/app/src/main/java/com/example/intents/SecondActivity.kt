package com.example.intents

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import kotlinx.android.synthetic.main.activity_first.btn_call
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        btn_call.isEnabled = false

        btn_call.setOnClickListener { loadThirdActivity() }

        et_enter_number.doAfterTextChanged { buttonCallEnable(it.isNullOrBlank()) }
    }

    private fun buttonCallEnable(status: Boolean) {
        btn_call.isEnabled = !status
    }

    private fun loadThirdActivity() {
        val intent = Intent(this, ThirdActivity::class.java)
        intent.putExtra("NUMBER", et_enter_number.text.toString().toInt())
        startActivity(intent)
    }
}
