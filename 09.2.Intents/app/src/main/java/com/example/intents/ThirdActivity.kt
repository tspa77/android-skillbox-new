package com.example.intents

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_third.*

class ThirdActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)

        val gettingNumber = intent.getIntExtra("NUMBER", 0)
        val result = checkNumber(gettingNumber)
        showResult(result)
    }

    private fun checkNumber(number: Int): String {
        return when {
            number > 100 -> getString(R.string.msg_big)
            number < 100 -> getString(R.string.msg_small)
            else -> getString(R.string.msg_congratulations)
        }
    }

    private fun showResult(text: String) {
        tv_label.text = text
    }
}
