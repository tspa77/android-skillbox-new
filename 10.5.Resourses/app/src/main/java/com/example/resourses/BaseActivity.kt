package com.example.resourses

import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity

open class BaseActivity : AppCompatActivity() {

    private val dayTimeReceiver = DayTimeReceiver()

    override fun onResume() {
        super.onResume()
        checkRelevanceTheme(this)
        this.registerReceiver(dayTimeReceiver, IntentFilter("android.intent.action.TIME_TICK"))
    }

    override fun onPause() {
        super.onPause()
        this.unregisterReceiver(dayTimeReceiver)
    }
}
