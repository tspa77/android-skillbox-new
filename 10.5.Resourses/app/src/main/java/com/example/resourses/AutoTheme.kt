package com.example.resourses

import android.content.Context
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatDelegate
import java.text.SimpleDateFormat
import java.util.*

val DAYTIME_PERIOD = 6 until 23

fun getHour() = SimpleDateFormat("H", Locale.getDefault()).format(Date()).toInt()

fun checkRelevanceTheme(context: Context) {
    val currentHour = getHour()
    val currentMode =
        when (context.resources.configuration.uiMode.and(Configuration.UI_MODE_NIGHT_MASK)) {
            Configuration.UI_MODE_NIGHT_NO -> AppCompatDelegate.MODE_NIGHT_NO
            Configuration.UI_MODE_NIGHT_YES -> AppCompatDelegate.MODE_NIGHT_YES
            else -> AppCompatDelegate.MODE_NIGHT_UNSPECIFIED
        }

    when {
        // если сейчас день, но установлен не дневной режим
        currentHour in DAYTIME_PERIOD && currentMode != AppCompatDelegate.MODE_NIGHT_NO
        -> setTheme(AppCompatDelegate.MODE_NIGHT_NO)
        //если сейчас ночь, но установлен не ночной режим
        currentHour !in DAYTIME_PERIOD && currentMode != AppCompatDelegate.MODE_NIGHT_YES
        -> setTheme(AppCompatDelegate.MODE_NIGHT_YES)
    }
}

fun setTheme(theme: Int) {
    AppCompatDelegate.setDefaultNightMode(theme)
}
