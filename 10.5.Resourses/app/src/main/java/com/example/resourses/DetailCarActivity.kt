package com.example.resourses

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_detail_car.*

class DetailCarActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_car)

        val id = intent.getIntExtra("ID", 0)
        showInfo(id)
    }

    private fun showInfo(id: Int) {
        val idImageResource = when (id) {
            0 -> R.drawable.vesta
            1 -> R.drawable.granta
            2 -> R.drawable.priora
            else -> R.drawable.logo
        }
        tv_car_name.text = resources.getStringArray(R.array.cars_names)[id]
        iv_car.setImageResource(idImageResource)
    }
}
