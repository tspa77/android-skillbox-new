package com.example.resourses

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btn_choose.setOnClickListener { loadDetailActivity() }

        button_TEST.setOnClickListener {



        }
    }


    private fun loadDetailActivity() {
        val intent = Intent(this, DetailCarActivity::class.java)
        intent.putExtra("ID", spin_car_names.selectedItemPosition.toString().toInt())
        startActivity(intent)
    }
}
