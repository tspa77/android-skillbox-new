package com.example.shoppinglist

import com.example.shoppinglist.model.Category
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

//    "Продукты", "Одежда", "Хоз.товары", "Инструменты"

    @Test
    fun saveCategory() {
        val listCategory = listOf(
            Category.makeCategory("Продукты"),
            Category.makeCategory("Одежда"),
            Category.makeCategory("Хоз.товары"),
            Category.makeCategory("Инструменты")
        )

        val listCategoryToString = listCategory.map { it.toString() }

        listCategoryToString.forEach { println(it) }
    }
}
