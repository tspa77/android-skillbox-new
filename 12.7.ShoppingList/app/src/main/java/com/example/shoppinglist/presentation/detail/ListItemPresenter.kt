package com.example.shoppinglist.presentation.detail

import com.example.shoppinglist.util.SPHelper
import com.example.shoppinglist.model.Item

class ListItemPresenter(
    private val view: ListItemActivity,
    private val categoryHash: Int
) {

    private var spHelper = SPHelper(view.applicationContext)

    fun loadListItem() = view.setData(spHelper.getItemList(categoryHash))

    fun addItem(item: Item) {
        val list = spHelper.getItemList(categoryHash)
        list.add(item)
        updateListItem(list)
    }

    fun renameItem(item: Item, newTitle: String) {
        val list = spHelper.getItemList(categoryHash)
        list.first { it == item }.title = newTitle
        updateListItem(list)
    }

    fun deleteItem(item: Item) {
        val list = spHelper.getItemList(categoryHash)
        list.remove(item)
        updateListItem(list)
    }

    fun changeStatusDone(item: Item, isChecked: Boolean) {
        val list = spHelper.getItemList(categoryHash)
        list.first { it == item }.isChecked = isChecked
        updateListItem(list)
    }

    private fun updateListItem(list: List<Item>) {
        spHelper.saveItemList(categoryHash, list)
        view.setData(list)
    }
}
