package com.example.shoppinglist.util

import android.content.Context
import android.content.Context.MODE_PRIVATE
import com.example.shoppinglist.common.AppConstants.LIST_CATEGORY
import com.example.shoppinglist.common.AppConstants.LIST_SIZE
import com.example.shoppinglist.model.Category
import com.example.shoppinglist.model.Item

class SPHelper(private val context: Context) {

    fun saveCategoryList(categoryList: List<Category>) {
        val sp = context.getSharedPreferences(LIST_CATEGORY, MODE_PRIVATE)
        val editor = sp.edit()
        val listCategoryToString = categoryList.map { it.toString() }
        editor.putStringSet(LIST_CATEGORY, listCategoryToString.toHashSet())
        editor.apply()
    }

    fun getCategoryList(): MutableList<Category> {
        val sp = context.getSharedPreferences(LIST_CATEGORY, MODE_PRIVATE)
        val listCategory = mutableListOf<Category>()

        // Выдача демо-списка если это первый запуск приложения
        if (!sp.contains(LIST_CATEGORY)) {
            listCategory.addAll(
                listOf(
                    Category.makeCategory("Продукты"),
                    Category.makeCategory("Одежда"),
                    Category.makeCategory("Хоз.товары"),
                    Category.makeCategory("Инструменты")
                )
            )
            saveCategoryList(listCategory)
        } else {
            val set = sp.getStringSet(LIST_CATEGORY, emptySet())
            set?.forEach {
                listCategory.add(Category.restoreCategory(it))
            }
        }
        return listCategory
    }

    fun saveItemList(categoryHash: Int, listItem: List<Item>) {
        val fileName = categoryHash.toString()
        val sp = context.getSharedPreferences(fileName, MODE_PRIVATE)
        val editor = sp.edit()

        listItem.forEachIndexed { index, item ->
            editor.putString(index.toString(), item.toString())
        }
        editor.putInt(LIST_SIZE, listItem.size)
        editor.apply()
    }

    fun getItemList(categoryHash: Int): MutableList<Item> {
        val fileName = categoryHash.toString()
        val sp = context.getSharedPreferences(fileName, MODE_PRIVATE)
        val listItem = mutableListOf<Item>()

        if (sp.contains(LIST_SIZE)) {
            val size = sp.getInt(LIST_SIZE, 0)
            for (i in 0 until size) {
                val stringItem = sp.getString(i.toString(), "")!!
                listItem.add(Item.restoreItem(stringItem))
            }
        }
        return listItem
    }
}
