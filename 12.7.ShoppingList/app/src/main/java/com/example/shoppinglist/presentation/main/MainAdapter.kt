package com.example.shoppinglist.presentation.main

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.example.shoppinglist.R
import com.example.shoppinglist.model.Category
import kotlinx.android.synthetic.main.card_category.view.*

class MainAdapter(
    private val context: Context,
    private val onItemClickListener: OnItemClickListener
) : RecyclerView.Adapter<MainAdapter.MainViewHolder>() {

    private var listCategory = listOf<Category>()

    interface OnItemClickListener {
        fun onItemClicked(category: Category)
        fun onMenuDeleteClicked(category: Category)
        fun onMenuRenameClicked(category: Category)
    }

    class MainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvCategory: TextView = itemView.tv_category
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.card_category, parent, false)
        val holder = MainViewHolder(view)

        view.setOnClickListener {
            val adapterPosition = holder.adapterPosition
            if (adapterPosition != RecyclerView.NO_POSITION) {
                onItemClickListener.onItemClicked(listCategory[adapterPosition])
            }
        }

        view.setOnLongClickListener {
            val adapterPosition = holder.adapterPosition
            if (adapterPosition != RecyclerView.NO_POSITION) {
                val popup = PopupMenu(context, view)
                popup.inflate(R.menu.main_context_menu)
                popup.gravity = Gravity.END
                popup.setOnMenuItemClickListener {
                    when (it.itemId) {
                        R.id.main_menu_delete ->
                            onItemClickListener.onMenuDeleteClicked(listCategory[adapterPosition])
                        R.id.item_menu_rename ->
                            onItemClickListener.onMenuRenameClicked(listCategory[adapterPosition])
                    }
                    return@setOnMenuItemClickListener true
                }
                popup.show()
            }
            return@setOnLongClickListener true
        }
        return holder
    }

    override fun getItemCount() = listCategory.size

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.tvCategory.text = listCategory[position].title
    }

    fun setData(list: List<Category>){
        listCategory = list
        notifyDataSetChanged()
    }
}
