package com.example.shoppinglist.presentation.main

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shoppinglist.common.AppConstants.CATEGORY_HASH
import com.example.shoppinglist.common.AppConstants.CATEGORY_TITLE
import com.example.shoppinglist.R
import com.example.shoppinglist.model.Category
import com.example.shoppinglist.presentation.detail.ListItemActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainAdapter.OnItemClickListener {

    private val mainAdapter = MainAdapter(this, this)
    private lateinit var mainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv_categories.adapter = mainAdapter
        rv_categories.layoutManager = LinearLayoutManager(this)

        mainPresenter = MainPresenter(this)
        mainPresenter.loadListCategory()

        fab_add_category.setOnClickListener { addCategory() }
    }

    private fun addCategory() {
        val alert: AlertDialog.Builder = AlertDialog.Builder(this)

        alert.setTitle("Новая категория")
        alert.setMessage("Введите название:")

        val etInput = EditText(this)
        alert.setView(etInput)

        alert.setPositiveButton("Готово") { _, _ ->
            val categoryTitle = etInput.text.toString()
            if (categoryTitle.trim().isNotEmpty()) {
                mainPresenter.addCategory(Category.makeCategory(categoryTitle))
            } else {
                Toast.makeText(
                    this, "Категория не может быть пустой", Toast.LENGTH_LONG
                ).show()
            }
        }
        alert.setNegativeButton("Отмена") { _, _ -> }
        alert.show()
    }

    override fun onItemClicked(category: Category) {
        val intent = Intent(this, ListItemActivity::class.java)
        intent.putExtra(CATEGORY_TITLE, category.title)
        intent.putExtra(CATEGORY_HASH, category.hash)
        startActivity(intent)
    }

    override fun onMenuDeleteClicked(category: Category) {
        mainPresenter.deleteCategory(category)
    }

    override fun onMenuRenameClicked(category: Category) {
        val alert: AlertDialog.Builder = AlertDialog.Builder(this)

        alert.setTitle("Переименовать категорию")
        alert.setMessage("Введите название:")

        val input = EditText(this)
        input.setText(category.title)
        alert.setView(input)

        alert.setPositiveButton("Готово") { _, _ ->
            val newTitle = input.text.toString()
            if (newTitle.trim().isNotEmpty()) {
                mainPresenter.renameCategory(category, newTitle.trim())
            } else {
                Toast.makeText(
                    this, "Категория не может быть пустой", Toast.LENGTH_LONG
                ).show()
            }
        }
        alert.setNegativeButton("Отмена") { _, _ -> }
        alert.show()
    }

    fun setData(list: List<Category>) {
        mainAdapter.setData(list)
    }
}
