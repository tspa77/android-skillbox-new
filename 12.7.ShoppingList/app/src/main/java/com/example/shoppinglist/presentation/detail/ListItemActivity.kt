package com.example.shoppinglist.presentation.detail

import android.app.AlertDialog
import android.os.Bundle
import android.os.Handler
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shoppinglist.R
import com.example.shoppinglist.common.AppConstants.CATEGORY_HASH
import com.example.shoppinglist.common.AppConstants.CATEGORY_TITLE
import com.example.shoppinglist.model.Item
import kotlinx.android.synthetic.main.activity_detail.*

class ListItemActivity : AppCompatActivity(), ListItemAdapter.OnItemClickListener {

    private val itemAdapter = ListItemAdapter(this, this)
    private lateinit var itemPresenter: ListItemPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        // Получаем текущую категорию
        val category = intent.getStringExtra(CATEGORY_TITLE) ?: ""
        title = category
        val categoryHash = intent.getIntExtra(CATEGORY_HASH, 0)

        rv_item.adapter = itemAdapter
        rv_item.layoutManager = LinearLayoutManager(this)

        itemPresenter = ListItemPresenter(this, categoryHash)
        itemPresenter.loadListItem()

        fab_add_item.setOnClickListener { addItem() }
    }

    private fun addItem() {
        val alert: AlertDialog.Builder = AlertDialog.Builder(this)

        alert.setTitle("Новая позиция")
        alert.setMessage("Введите название:")

        val etInput = EditText(this)
        alert.setView(etInput)

        alert.setPositiveButton("Готово") { _, _ ->
            val itemTitle = etInput.text.toString()
            if (itemTitle.trim().isNotEmpty()) {
                itemPresenter.addItem(Item.makeItem(itemTitle))
            } else {
                Toast.makeText(
                    this, "Название не может быть пустым", Toast.LENGTH_LONG
                ).show()
            }
        }
        alert.setNegativeButton("Отмена") { _, _ -> }
        alert.show()
    }

    override fun onCheckBoxClick(item: Item, isChecked: Boolean) {
        itemPresenter.changeStatusDone(item, isChecked)
    }

    override fun onMenuDeleteClicked(item: Item) {
        itemPresenter.deleteItem(item)
    }

    override fun onMenuRenameClicked(item: Item) {
        val alert: AlertDialog.Builder = AlertDialog.Builder(this)

        alert.setTitle("Переименовать категорию")
        alert.setMessage("Введите название:")

        val input = EditText(this)
        input.setText(item.title)
        alert.setView(input)

        alert.setPositiveButton("Готово") { _, _ ->
            val newTitle = input.text.toString()
            if (newTitle.trim().isNotEmpty()) {
                itemPresenter.renameItem(item, newTitle.trim())
            } else {
                Toast.makeText(
                    this, "Категория не может быть пустой", Toast.LENGTH_LONG
                ).show()
            }
        }
        alert.setNegativeButton("Отмена") { _, _ -> }
        alert.show()
    }

    fun setData(list: List<Item>) {
        Handler().post {
            itemAdapter.setData(list)
        }
        // Обернуто в хэндлер т.к. иначе E/AndroidRuntime: FATAL EXCEPTION: main
        // java.lang.IllegalStateException: Cannot call this method while RecyclerView is computing
        // a layout or scrolling androidx.recyclerview.widget.RecyclerView
    }
}
