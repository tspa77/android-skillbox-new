package com.example.shoppinglist.model

import com.example.shoppinglist.common.AppConstants.SEPARATOR

data class Item private constructor(
    private val hash: Int,
    var title: String,
    var isChecked: Boolean
) {

    override fun toString(): String {
        return "$hash$SEPARATOR$title$SEPARATOR$isChecked"
    }

    companion object Factory {
        fun makeItem(title: String): Item {
            val hash = (title.hashCode() * Math.random()).toInt()
            return Item(hash, title, false)
        }

        fun restoreItem(spString: String): Item {
            val (hash, title, isChecked) = spString.split("||")
            return Item(hash.toInt(), title, isChecked.toBoolean())
        }
    }
}
