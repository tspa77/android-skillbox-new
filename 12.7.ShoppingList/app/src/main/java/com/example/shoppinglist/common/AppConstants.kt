package com.example.shoppinglist.common

object AppConstants {
    const val CATEGORY_TITLE = "Category title"
    const val CATEGORY_HASH = "Category hash"
    const val SEPARATOR = "||"
    const val LIST_CATEGORY = "category"
    const val LIST_SIZE = "size"
}