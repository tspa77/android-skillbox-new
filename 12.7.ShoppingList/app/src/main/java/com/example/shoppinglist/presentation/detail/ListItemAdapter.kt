package com.example.shoppinglist.presentation.detail

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.PopupMenu
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.shoppinglist.R
import com.example.shoppinglist.model.Item
import kotlinx.android.synthetic.main.card_item.view.*

class ListItemAdapter(
    private val context: Context,
    private val onItemClickListener: OnItemClickListener
) : RecyclerView.Adapter<ListItemAdapter.DetailsViewHolder>() {

    private var listItem = listOf<Item>()

    interface OnItemClickListener {
        fun onCheckBoxClick(item: Item, isChecked: Boolean)
        fun onMenuRenameClicked(item: Item)
        fun onMenuDeleteClicked(item: Item)
    }

    class DetailsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cbDone: CheckBox = itemView.cb_done
        val tvItemTitle: TextView = itemView.tv_item_title
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailsViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.card_item, parent, false)
        val holder = DetailsViewHolder(view)

        view.setOnClickListener {
            val adapterPosition = holder.adapterPosition
            if (adapterPosition != RecyclerView.NO_POSITION) {
                view.cb_done.isChecked = !view.cb_done.isChecked
            }
        }

        view.cb_done.setOnCheckedChangeListener { _, isChecked ->
            val adapterPosition = holder.adapterPosition
            if (adapterPosition != RecyclerView.NO_POSITION) {
                onItemClickListener.onCheckBoxClick(listItem[adapterPosition], isChecked)
            }
        }

        view.tv_options.setOnClickListener {
            val adapterPosition = holder.adapterPosition
            val popup = PopupMenu(context, view)
            popup.inflate(R.menu.item_context_menu)
            popup.gravity = Gravity.END
            popup.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.item_menu_rename ->
                        onItemClickListener.onMenuRenameClicked(listItem[adapterPosition])
                    R.id.item_menu_delete->
                        onItemClickListener.onMenuDeleteClicked(listItem[adapterPosition])
                }
                return@setOnMenuItemClickListener true
            }
            popup.show()
        }
        return holder
    }

    override fun getItemCount(): Int = listItem.size

    override fun onBindViewHolder(holder: DetailsViewHolder, position: Int) {
        holder.tvItemTitle.text = listItem[position].title
        holder.cbDone.isChecked = listItem[position].isChecked
    }

    fun setData(list: List<Item>) {
        listItem = list
        notifyDataSetChanged()
    }
}
