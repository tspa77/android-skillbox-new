package com.example.shoppinglist.presentation.main

import com.example.shoppinglist.util.SPHelper
import com.example.shoppinglist.model.Category

class MainPresenter(private val view: MainActivity) {

    private val spHelper = SPHelper(view.applicationContext)

    fun loadListCategory() = view.setData(spHelper.getCategoryList())

    fun addCategory(category: Category) {
        val list = spHelper.getCategoryList()
        list.add(category)
        updateListCategory(list)
    }

    fun renameCategory(category: Category, newTitle: String) {
        val list = spHelper.getCategoryList()
        list.first { it == category }.title = newTitle
        updateListCategory(list)
    }

    fun deleteCategory(category: Category) {
        val list = spHelper.getCategoryList()
        list.remove(category)
        updateListCategory(list)
    }

    private fun updateListCategory(list: List<Category>) {
        spHelper.saveCategoryList(list)
        view.setData(list)
    }
}
