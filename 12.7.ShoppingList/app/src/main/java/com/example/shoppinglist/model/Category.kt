package com.example.shoppinglist.model

import com.example.shoppinglist.common.AppConstants.SEPARATOR

data class Category private constructor(
    val hash: Int,
    var title: String
) {

    override fun toString(): String {
        return "$hash$SEPARATOR$title"
    }

    companion object Factory {
        fun makeCategory(title: String): Category {
            val hash = (title.hashCode() * Math.random()).toInt()
            return Category(hash, title)
        }

        fun restoreCategory(spString: String): Category {
            val (hash, title) = spString.split("||")
            return Category(hash.toInt(), title)
        }
    }
}
