package com.example.startactivityforresult

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.startactivityforresult.AppConstants.CODE_FIRST_NAME
import com.example.startactivityforresult.AppConstants.CODE_FOR_CONTACT_ACTIVITY
import com.example.startactivityforresult.AppConstants.CODE_FOR_GROUP_ACTIVITY
import com.example.startactivityforresult.AppConstants.CODE_GROUP
import com.example.startactivityforresult.AppConstants.CODE_LAST_NAME
import com.example.startactivityforresult.AppConstants.CODE_PHONE_NUMBER
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_create_contact.setOnClickListener { loadContactActivity() }
        btn_change_group.setOnClickListener { loadGroupActivity() }
    }


    private fun loadContactActivity() {
        val intent = Intent(this, ContactActivity::class.java)
        startActivityForResult(intent, CODE_FOR_CONTACT_ACTIVITY)
    }

    private fun loadGroupActivity() {
        val intent = Intent(this, GroupActivity::class.java)
        startActivityForResult(intent, CODE_FOR_GROUP_ACTIVITY)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CODE_FOR_CONTACT_ACTIVITY) {
            when (resultCode) {
                Activity.RESULT_CANCELED -> showWarning()
                Activity.RESULT_OK -> fillContactFields(data)
            }
        }

        if (requestCode == CODE_FOR_GROUP_ACTIVITY && resultCode == Activity.RESULT_OK) {
            setGroup(data)
        }
    }

    private fun showWarning() {
        Toast.makeText(this, "Данные не сохранены!", Toast.LENGTH_LONG).show()
    }

    private fun fillContactFields(data: Intent?) {
        tv_first_name.text = data?.getStringExtra(CODE_FIRST_NAME) ?: "error"
        tv_first_name.setTextColor(Color.parseColor("#8A2BE2"))
        tv_last_name.text = data?.getStringExtra(CODE_LAST_NAME) ?: "error"
        tv_last_name.setTextColor(Color.parseColor("#8A2BE2"))
        tv_phone_number.text = data?.getStringExtra(CODE_PHONE_NUMBER) ?: "error"
        tv_phone_number.setTextColor(Color.parseColor("#8A2BE2"))
    }

    private fun setGroup(data: Intent?) {
        tv_group.text = data?.getStringExtra(CODE_GROUP) ?: "error"
        tv_group.setTextColor(Color.parseColor("#8A2BE2"))
    }
}
