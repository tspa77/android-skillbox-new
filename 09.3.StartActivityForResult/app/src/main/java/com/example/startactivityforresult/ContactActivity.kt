package com.example.startactivityforresult

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.startactivityforresult.AppConstants.CODE_FIRST_NAME
import com.example.startactivityforresult.AppConstants.CODE_LAST_NAME
import com.example.startactivityforresult.AppConstants.CODE_PHONE_NUMBER
import kotlinx.android.synthetic.main.activity_contact.*

class ContactActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact)

        btn_ok.setOnClickListener { clickButtonOk() }
    }

    private fun clickButtonOk() {
        if (isBlankField()) {
            Toast.makeText(
                this, "Все поля должны быть заполнены", Toast.LENGTH_LONG
            ).show()
        } else {
            onCloseActivity()
        }
    }

    private fun onCloseActivity() {
        val intentToFirstActivity = Intent()
        intentToFirstActivity.putExtra(CODE_FIRST_NAME, et_first_name.text.toString())
        intentToFirstActivity.putExtra(CODE_LAST_NAME, et_last_name.text.toString())
        intentToFirstActivity.putExtra(CODE_PHONE_NUMBER, et_phone_number.text.toString())
        setResult(Activity.RESULT_OK, intentToFirstActivity)
        this.finish()
    }

    private fun isBlankField(): Boolean {
        return et_first_name.text.isBlank() ||
                et_last_name.text.isBlank() ||
                et_phone_number.text.isBlank()
    }
}
