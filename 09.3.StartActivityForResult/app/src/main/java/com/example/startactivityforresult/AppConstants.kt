package com.example.startactivityforresult

object AppConstants {
    val CODE_FOR_CONTACT_ACTIVITY = 42
    val CODE_FOR_GROUP_ACTIVITY = 69
    val CODE_FIRST_NAME = "CODE_FIRST_NAME"
    val CODE_LAST_NAME = "CODE_LAST_NAME"
    val CODE_PHONE_NUMBER = "CODE_PHONE_NUMBER"
    val CODE_GROUP = "CODE_GROUP"
}