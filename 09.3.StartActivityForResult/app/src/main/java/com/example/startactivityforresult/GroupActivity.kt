package com.example.startactivityforresult

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_contact.btn_ok
import kotlinx.android.synthetic.main.activity_group.*

class GroupActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group)

        btn_ok.setOnClickListener { onCloseActivity() }
    }


    private fun onCloseActivity() {
        val group = et_group.text.trim().toString()

        if (group.isBlank()) {
            setResult(Activity.RESULT_CANCELED)
        } else {
            val intentToFirstActivity = Intent()
            intentToFirstActivity.putExtra(AppConstants.CODE_GROUP, group)
            setResult(Activity.RESULT_OK, intentToFirstActivity)
        }
        this.finish()
    }
}
