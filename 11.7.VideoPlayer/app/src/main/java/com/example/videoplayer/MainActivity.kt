package com.example.videoplayer

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.WindowManager
import android.widget.MediaController
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val POSITION = "POSITION"
    private var position = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Fullscreen
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        supportActionBar?.hide()

        // Set path to videofile
        val pathToVideoFile = "android.resource://com.example.videoplayer/" + R.raw.trailer
        videoview_screen.setVideoPath(pathToVideoFile)

        // Restore position
        position = savedInstanceState?.getInt(POSITION) ?: 0

        // MediaController with navigation bar
        val mediaController = MediaController(this)
        mediaController.setAnchorView(videoview_screen)
        videoview_screen.setMediaController(mediaController)

        // Fullscreen video
        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)
        val params = videoview_screen.layoutParams as ConstraintLayout.LayoutParams
        params.width = metrics.widthPixels
        params.height = metrics.heightPixels
        videoview_screen.layoutParams = params
    }


    override fun onResume() {
        super.onResume()

        videoview_screen.seekTo(position)
        videoview_screen.start()
    }

    override fun onPause() {
        super.onPause()

        position = videoview_screen.currentPosition
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt(POSITION, position)
    }
}
