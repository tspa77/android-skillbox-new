package com.example.a0807baseui_constraint

import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val NAME = "NAME"
    private val GROUP = "GROUP"
    private val MESSAGE = "MESSAGE"
    private lateinit var mapAllUsers: MutableMap<String, List<String>>
    private lateinit var adapterSpinnerGroups: ArrayAdapter<String>
    private lateinit var adapterSpinnerUsers: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mapAllUsers = getAllUsers()

        btnSend.setOnClickListener { sendMessage() }

        etMessage.doOnTextChanged { text, _, _, _ ->
            run { tvMessage.text = text }
        }

        spinnerInit()
    }

    private fun spinnerInit() {
        val listGroups = ArrayList(mapAllUsers.keys)
        adapterSpinnerGroups = ArrayAdapter(this, android.R.layout.simple_spinner_item, listGroups)
        spGroups.adapter = adapterSpinnerGroups

        val selectedGroup = spGroups.selectedItem.toString()
        val listUsers = ArrayList(mapAllUsers[selectedGroup].orEmpty())
        adapterSpinnerUsers = ArrayAdapter(this, android.R.layout.simple_spinner_item, listUsers)
        spUsers.adapter = adapterSpinnerUsers


        spGroups.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, view: View?, position: Int, id: Long
            ) {
                loadUsers(spGroups.selectedItem.toString())
                tvGroup.text = spGroups.selectedItem.toString()
                tvName.text = spUsers.selectedItem.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        spUsers.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?, view: View?, position: Int, id: Long
            ) {
                tvName.text = spUsers.selectedItem.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun loadUsers(key: String) {
        val listUsers = mapAllUsers[key].orEmpty()

        adapterSpinnerUsers.apply {
            clear()
            addAll(listUsers)
            setNotifyOnChange(true)
        }
    }

    private fun sendMessage() {
        val toast = Toast.makeText(applicationContext, "Сообщение отправлено", Toast.LENGTH_LONG)
        toast.apply {
            setGravity(Gravity.CENTER, 0, 0)
            show()
        }
        etMessage.text.clear()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putString(NAME, tvName.text.toString())
            putString(GROUP, tvGroup.text.toString())
            putString(MESSAGE, tvMessage.text.toString())
        }
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        tvName.text = savedInstanceState.getString(NAME)
        tvGroup.text = savedInstanceState.getString(GROUP)
        tvMessage.text = savedInstanceState.getString(MESSAGE)
        super.onRestoreInstanceState(savedInstanceState)
    }
}
