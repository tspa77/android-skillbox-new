package com.example.a0807baseui_constraint

fun getAllUsers(): MutableMap<String, List<String>> {
    val listUsersCarGroup = listOf("Анна", "Андрей", "Антон", "Александр")
    val listUsersBikeGroup = listOf("Борис", "Бенедект", "Бриджит", "Бонифаций")
    val listUsersBicycleGroup = listOf("Виктор", "Владимир", "Вероника", "Василий")
    return mutableMapOf(
        "Автомобилисты" to listUsersCarGroup,
        "Мотоциклисты" to listUsersBikeGroup,
        "Велосипедисты" to listUsersBicycleGroup
    )
}
