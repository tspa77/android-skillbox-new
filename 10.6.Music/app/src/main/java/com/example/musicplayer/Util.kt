package com.example.musicplayer

fun getSingersAndSong(text: String): Pair<String, String> {
    val (singer, song) = text.split("__")
    return formatFirstCharCapitalize(singer) to formatFirstCharCapitalize(song)
}

fun formatFirstCharCapitalize(text: String): String {
    val words = text.split("_")
    return words.joinToString(" ") { it.capitalize() }
}
