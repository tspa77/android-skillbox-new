package com.example.musicplayer

interface PlayerView {
    fun showCover(id: Int)

    fun showInfo(info: Pair<String, String>)

    fun updatePlayButton(isPlaying: Boolean)
}
