package com.example.musicplayer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), PlayerView {

    private lateinit var mp3Player: Mp3Player

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mp3Player = Mp3Player(this)

        ib_btn_prev.setOnClickListener { mp3Player.prev() }
        ib_btn_play_pause.setOnClickListener { mp3Player.play() }
        ib_btn_next.setOnClickListener { mp3Player.next() }
    }

    override fun showCover(id: Int) {
        iv_cover.setImageResource(id)
    }

    override fun showInfo(info: Pair<String, String>) {
        tv_artist.text = info.first
        tv_song.text = info.second
    }

    override fun updatePlayButton(isPlaying: Boolean) {
        ib_btn_play_pause.setImageResource(
            if (isPlaying) {
                R.drawable.ic_pause_black_48dp
            } else {
                R.drawable.ic_play_arrow_black_48dp
            }
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        mp3Player.release()
    }
}
