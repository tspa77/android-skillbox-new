package com.example.musicplayer

import android.media.MediaPlayer

class Mp3Player(private val view: PlayerView) {

    private lateinit var mediaPlayer: MediaPlayer
    private val appContext = MyApp.applicationContext()
    private var isPlaying = false
    private var currentNumberInPL = 0
    private var playListIdResRaw: MutableList<Int>

    init {
        playListIdResRaw = loadPlayList()
        setSongToPlayer(currentNumberInPL)
        updateView()
    }

    private fun setSongToPlayer(idSongInPlaylist: Int) {
        mediaPlayer = MediaPlayer.create(appContext, playListIdResRaw[idSongInPlaylist])
        mediaPlayer.setOnCompletionListener { next() }
    }

    fun play() {
        isPlaying = if (!isPlaying) {
            mediaPlayer.start()
            true
        } else {
            mediaPlayer.pause()
            false
        }
        view.updatePlayButton(isPlaying)
    }


    fun next() {
        if (currentNumberInPL == playListIdResRaw.size - 1) {
            currentNumberInPL = 0
        } else {
            currentNumberInPL++
        }
        switchSong()
    }

    fun prev() {
        if (currentNumberInPL == 0) {
            currentNumberInPL = playListIdResRaw.size - 1
        } else {
            currentNumberInPL--
        }
        switchSong()
    }

    private fun switchSong() {
        mediaPlayer.stop()
        setSongToPlayer(currentNumberInPL)
        updateView()
        if (isPlaying) {
            isPlaying = false
            play()
        }
    }

    private fun updateView() {
        val singerAndSong = getFileNameFromSongID(playListIdResRaw[currentNumberInPL])
        view.showInfo(getSingersAndSong(singerAndSong))
        val coverId = getImageIdFromSongId(playListIdResRaw[currentNumberInPL])
        view.showCover(coverId)
    }

    private fun getImageIdFromSongId(songId: Int): Int {
        val fileName = getFileNameFromSongID(songId)
        return appContext.resources.getIdentifier(fileName, "drawable", appContext.packageName)
    }

    private fun getFileNameFromSongID(songId: Int): String {
        return appContext.resources.getResourceEntryName(songId)
    }

    private fun loadPlayList(): MutableList<Int> {
        return mutableListOf(
            R.raw.aerosmith__dream_on,
            R.raw.black_sabbath__paranoid,
            R.raw.creedence_clearwater_revival__fortunate_son,
            R.raw.nirvana__smells_like_teen_spirit,
            R.raw.roxy_music__if_there_is_something,
            R.raw.shocking_blue__beggin
        )
    }

    fun release() {
        mediaPlayer.release()
    }
}
