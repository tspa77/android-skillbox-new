package com.example.implicitintent

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_about.*


class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        btn_to_site.setOnClickListener { goToSite() }
    }

    private fun goToSite() {
        val address = Uri.parse("https://kotlinlang.ru")
        val openLinkIntent = Intent(Intent.ACTION_VIEW, address)

        if (openLinkIntent.resolveActivity(packageManager) != null) {
            startActivity(openLinkIntent)
        } else {
            Log.d("Intent", "Не получается обработать намерение!")
        }
    }
}
