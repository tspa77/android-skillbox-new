package com.example.voiceassistance.Servises;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.function.Consumer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class Advice {

    public static class ApiResult {
        @SerializedName("text")
        public String text;
    }


    public static void getAdvice(final Consumer<String> callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://fucking-great-advice.ru")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Call<ApiResult> call = retrofit
                .create(AdviceServise.class)
                .getAdvice();

        call.enqueue(new Callback<ApiResult>() {
            @Override
            public void onResponse(Call<ApiResult> call, Response<ApiResult> response) {
                ApiResult result = response.body();
                callback.accept("Вот тебе мой совет: " + result.text);
            }

            @Override
            public void onFailure(Call<ApiResult> call, Throwable t) {
                Log.w("WEATHER", t.getMessage());
                callback.accept("Даже не знаю, что тебе посоветовать... :(");
            }
        });
    }


    public interface AdviceServise {
        @GET("http://fucking-great-advice.ru/api/random/")
        Call<ApiResult> getAdvice();
    }


}
