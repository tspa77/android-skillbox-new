package com.example.voiceassistance;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.function.Consumer;

public class MainActivity extends AppCompatActivity {

    private TextView chatWindowTextView;
    private EditText questionFieldEditText;
    private Button sendButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        sendButton.setOnClickListener(v -> onClickSendButton());
    }

    private void onClickSendButton() {
        String text = questionFieldEditText.getText().toString().trim();
        chatWindowTextView.append(">> " + text + "\n");
        AI.getAnswer(text, answer -> chatWindowTextView.append("<< " + answer + "\n"));
        questionFieldEditText.setText("");
    }

    private void initView() {
        chatWindowTextView = findViewById(R.id.chatWindowTextView);
        questionFieldEditText = findViewById(R.id.questionFieldEditText);
        sendButton = findViewById(R.id.sendButton);


        // для быстрой отладки
        questionFieldEditText.setText("Привет! Как дела, какая погода в городе Томск? Дай совет и это, скажи какой сегодня день и сколько сейчас времени!!!");
        onClickSendButton();
    }
}
