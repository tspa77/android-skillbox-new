package com.example.voiceassistance;

import com.example.voiceassistance.Servises.Advice;
import com.example.voiceassistance.Servises.Weather;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AI {

    public static void getAnswer(String userQuestion, final Consumer<String> callback) {

        Map<String, String> database = new HashMap<>();
        database.put("привет", "Здрасте");
        database.put("как дела", "Дела норм");
        database.put("чем занимаешься", "Отвечаю на дурацкие вопросы");
        database.put("как тебя зовут", "Меня зовут Инокентий");
        database.put("кто тебя создал", "Создал меня программист");
        database.put("есть ли жизнь на марсе", "А это, на самом деле, довольно сложный вопрос...");
        database.put("кто президент россии", "Это и так все знают. Интересно кто следующий...");
        database.put("кто следующий президент россии", "А что за вопрос такой, " +
                "тебя чем-то нынешний не устраивает?");
        database.put("какого цвета небо", "Обычно голубое, но всё от погоды зависит...");

        userQuestion = userQuestion.toLowerCase();


//  ***** Сначала внешние запросы ****

//  Убрал добавление в ответ из коллбеков, так как в сложных запросах приходила 2 ответа.
//  Сначала из своей 'базы', а потом они же плюс каллбек. Теперь калбэки не плюсуются
//  в общий ответ answers. Чтобы было ясней, где на что отвечают подправил форму ответа калбэков

        //***** какая погода в городе ******
        Pattern cityPattern = Pattern.compile(
                "какая погода в городе (\\p{L}+)", Pattern.CASE_INSENSITIVE);
        Matcher matcher = cityPattern.matcher(userQuestion);

        if (matcher.find()) {
            String cityName = matcher.group(1);
            Weather.getWeather(cityName, callback);
        }

        //***** совет ******
        if (userQuestion.contains("совет")) {
            Advice.getAdvice(callback);
        }


//  ***** Теперь локальные 'базы' и запросы ****
        List<String> answers = new ArrayList<>();

        //***** база
        for (String databaseQuestion : database.keySet()) {
            if (userQuestion.contains(databaseQuestion)) {
                answers.add(database.get(databaseQuestion));
            }
        }

        //***** какой сегодня день ******
        if (userQuestion.contains("какой сегодня день")) {
            LocalDate date = LocalDate.now();
            String dayOfWeek = date.getDayOfWeek()
                    .getDisplayName(TextStyle.FULL, Locale.getDefault());
            String month = date.getMonth().getDisplayName(TextStyle.FULL, Locale.getDefault());
            String number = String.valueOf(date.getDayOfMonth());
            String nowDayAnswer = "Сегодня "
                    .concat(dayOfWeek).concat(", ").concat(number).concat("-е ").concat(month);
            answers.add(nowDayAnswer);
        }

        //***** сколько сейчас времени ******
        if (userQuestion.contains("сколько сейчас времени")) {
            LocalTime time = LocalTime.now();
            String nowTimeAnswer = "Насчёт времени - глянь в строке уведомления твоего телефона, " +
                    " там вроде написано, что " + time.getHour() + ":" + time.getMinute();
            answers.add(nowTimeAnswer);
        }

        if (answers.isEmpty()) {
            answers.add("Ok");
        }
        callback.accept(String.join(". ", answers));
    }
}
