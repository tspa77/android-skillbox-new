package com.example.voiceassistance.Servises;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.function.Consumer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class Weather {
    public static class Condition {
        @SerializedName("text")
        public String text;
    }

    public static class Forecast {
        @SerializedName("temp_c")
        public Float temperature;

        @SerializedName("condition")
        public Condition condition;
    }

    public static class ApiResult {
        @SerializedName("current")
        public Forecast current;
    }

    public interface WeatherServise {
        @GET("http://api.apixu.com/v1/current.json?key=eca8824b936b45b0ae351759190204")
        Call<ApiResult> getCurrentWeather(@Query("q") String city, @Query("lang") String lang);
    }

    public static String firstUpperCase(String word) {
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }


    public static void getWeather(String city, final Consumer<String> callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.apixu.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Call<ApiResult> call = retrofit
                .create(WeatherServise.class)
                .getCurrentWeather(city, "ru");

        call.enqueue(new Callback<ApiResult>() {
            @Override
            public void onResponse(Call<ApiResult> resultCall, Response<ApiResult> response) {
                ApiResult result = response.body();
                String answer = "В городе " + firstUpperCase(city) + " сейчас "
                        + result.current.condition.text.toLowerCase() +
                        ", где-то " + result.current.temperature.intValue() + " градусов";
                callback.accept(answer);
            }

            @Override
            public void onFailure(Call<ApiResult> call, Throwable t) {
                Log.w("WEATHER", t.getMessage());
            }
        });

    }
}
