package com.example.player.domain

import android.media.MediaPlayer
import android.net.Uri
import android.os.Handler
import com.example.player.domain.Player.Status.*
import com.example.player.model.Track
import com.example.player.presentation.MainActivity

class Player(private val view: MainActivity) {

    private lateinit var myUri: Uri
    private lateinit var mediaPlayer: MediaPlayer
    private lateinit var currentTrack: Track
    private var status = IsEmpty

    private lateinit var runnable: Runnable
    private var handler: Handler = Handler()


    enum class Status {
        IsPlaying, IsPaused, IsStopped, IsEmpty
    }

    fun setTrack(newTrack: Track) {
        if (status == IsPlaying || status == IsPaused) {
            toStop()
        } else {
            status = IsStopped
        }
        currentTrack = newTrack
    }

    private fun init() {
        myUri = currentTrack.uri
        mediaPlayer = MediaPlayer().apply {
            setDataSource(view.applicationContext, myUri)
            prepare()
        }
        view.seekBarSetMax(mediaPlayer.duration)
        view.uiEnable()
        status = IsStopped
    }

    private fun updateTimeStatus() {
        runnable = Runnable {
            if (status == IsPlaying) {
                view.setPlayingProgress(mediaPlayer.currentPosition)
                handler.postDelayed(runnable, 1000)
            } else {
                handler.removeCallbacks(runnable)
            }
        }
        handler.postDelayed(runnable, 1000)
    }

    fun onPlayPauseClicked() {
        when (status) {
            IsPlaying -> {
                mediaPlayer.pause()
                view.btnPPSetImagePlay()
                status = IsPaused
            }

            IsPaused -> {
                toPlay()
            }

            IsStopped -> {
                init()
                toPlay()
            }

            IsEmpty -> {
                view.uiDisable()
            }
        }
    }

    fun onStopClicked() {
        toStop()
    }

    fun seekTo(position: Int) {
        if (status == IsPlaying || status == IsPaused) {
            mediaPlayer.seekTo(position)
        }
    }

    private fun toPlay() {
        mediaPlayer.start()
        view.setPlayingProgress(mediaPlayer.currentPosition)
        view.btnPPSetImagePause()
        updateTimeStatus()
        status = IsPlaying
    }

    private fun toStop() {
        if (status == IsPlaying || status == IsPaused) {
            with(mediaPlayer) {
                stop()
                reset()
                release()
            }
            view.setPlayingProgress(0)
            status = IsStopped
            view.btnPPSetImagePlay()
        }
    }
}
