package com.example.player.presentation

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.player.R
import com.example.player.domain.Player
import com.example.player.extensions.formatByteToTextMb
import com.example.player.extensions.formatMillisecondsToTextMmSs
import com.example.player.model.Track
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), MainAdapter.OnItemClickListener {

    private lateinit var mainPresenter: MainPresenter
    private val mainAdapter = MainAdapter(this, this)
    private var player = Player(this)

    private val PERMISSION_REQUEST_CODE = 42

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv_playlist.adapter = mainAdapter
        rv_playlist.layoutManager = LinearLayoutManager(this)
        val mDividerItemDecoration = DividerItemDecoration(
            rv_playlist.context,
            LinearLayout.VERTICAL
        )
        rv_playlist.addItemDecoration(mDividerItemDecoration)

        ib_play_pause.setOnClickListener { player.onPlayPauseClicked() }
        ib_stop.setOnClickListener { player.onStopClicked() }

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    player.seekTo(progress)
                }
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })

        mainPresenter = MainPresenter(this)
        loadPlaylist()
    }

    private fun loadPlaylist() {
        if (havePermission()) {
            mainPresenter.getListTrack()
        } else {
            requestPermissionWithRationale()
        }
    }

    private fun havePermission() =
        checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED

    private fun requestPermissionWithRationale() {
        // Should we show an explanation?
        if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
        ) {
            val message = "Необходимо предоставить права для доступа к музыкальным файлам"
            val view = constraintlayout
            Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE).setAction("ОК") {
                requestReadExtStorage()
            }.show()
        } else {
            requestReadExtStorage()
        }
    }

    private fun requestReadExtStorage() = requestPermissions(
        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
        PERMISSION_REQUEST_CODE
    )

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_CODE) {
            for (i in permissions.indices) {
                if (permissions[i] == Manifest.permission.READ_EXTERNAL_STORAGE
                    && grantResults[i] == PackageManager.PERMISSION_DENIED
                ) {
                    Toast.makeText(
                        this,
                        """Любуйтесь интерфейсом, 
                            |проигрывать то нечего ¯\_(ツ)_/¯""".trimMargin(),
                        Toast.LENGTH_LONG
                    ).show()
                    uiDisable()
                } else {
                    mainPresenter.getListTrack()
                }
            }
        }
    }

    fun setData(list: List<Track>) {
        mainAdapter.setData(list)
        if (list.isNotEmpty()) {
            setTrack(list[0])
            uiEnable()
        } else {
            uiDisable()
        }
    }

    override fun onPlayListItemClicked(track: Track) {
        setTrack(track)
        player.onPlayPauseClicked()
    }

    private fun setTrack(track: Track) {
        tv_artist.text = track.artist
        tv_song_title.text = track.title
        tv_time_current.text = "00:00"
        tv_time_total.text = track.duration.formatMillisecondsToTextMmSs()
        tv_display_name.text = track.displayName
        tv_size.text = track.size.formatByteToTextMb()
        player.setTrack(track)
    }

    fun btnPPSetImagePause() = ib_play_pause.setImageResource(R.drawable.ic_pause_black_48dp)

    fun btnPPSetImagePlay() = ib_play_pause.setImageResource(R.drawable.ic_play_arrow_black_48dp)

    fun seekBarSetMax(length: Int) {
        seekBar.max = length
    }

    fun setPlayingProgress(currentPosition: Int) {
        seekBar.progress = currentPosition
        tv_time_current.text = currentPosition.formatMillisecondsToTextMmSs()
    }

    fun uiDisable() {
        // TODO change color to gray
        ib_stop.isEnabled = false
        ib_play_pause.isEnabled = false
        seekBar.isEnabled = false
    }

    fun uiEnable() {
        ib_stop.isEnabled = true
        ib_play_pause.isEnabled = true
        seekBar.isEnabled = true
    }
}
