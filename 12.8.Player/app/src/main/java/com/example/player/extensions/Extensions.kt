package com.example.player.extensions

import android.content.ContentUris
import android.content.Context
import android.provider.MediaStore
import com.example.player.model.Track
import java.util.concurrent.TimeUnit

/**
 * Extension method to get all music files list from external storage/sd card
 */
fun Context.musicFiles(): List<Track> {
    val selection = MediaStore.Audio.Media.IS_MUSIC + " != 0"
    val projection = arrayOf(
        MediaStore.Audio.Media._ID,
        MediaStore.Audio.Media.ARTIST,
        MediaStore.Audio.Media.TITLE,
        MediaStore.Audio.Media.DISPLAY_NAME,
        MediaStore.Audio.Media.DURATION,
        MediaStore.Audio.Media.SIZE
    )
    val sortOrder = "${MediaStore.Audio.Media.DISPLAY_NAME} ASC"

    val cursor = this.contentResolver.query(
        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
        projection,
        selection,
        null,
        sortOrder
    )

    val listTrack = mutableListOf<Track>()

    cursor?.let {
        // Cache column indices
        val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media._ID)
        val artistColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST)
        val titleColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE)
        val nameColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DISPLAY_NAME)
        val durationColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION)
        val sizeColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.SIZE)

        if (it.moveToFirst()) {
            do {
                // Get values of columns
                val id = cursor.getLong(idColumn)
                listTrack.add(
                    Track(
                        cursor.getString(artistColumn),
                        cursor.getString(titleColumn),
                        cursor.getString(nameColumn),
                        cursor.getInt(durationColumn),
                        cursor.getInt(sizeColumn),
                        ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id)
                    )
                )
            } while (it.moveToNext())
        }
    }
    cursor?.close()
    return listTrack
}

/**
 * Extension method for translating milliseconds into text time format
 */
fun Int.formatMillisecondsToTextMmSs(): String {
    val min = TimeUnit.MINUTES.convert(this.toLong(), TimeUnit.MILLISECONDS)
    val sec = TimeUnit.SECONDS.convert(this.toLong(), TimeUnit.MILLISECONDS) - min * 60
    return String.format("%02d:%02d", min.toInt(), sec.toInt())
}

/**
 * Extension method for translating bytes into byte and text format
 */
fun Int.formatByteToTextMb(): String {
    val size = (this.toFloat() / 1024 / 1024)
    return "%.2f Mb".format(size)
}



