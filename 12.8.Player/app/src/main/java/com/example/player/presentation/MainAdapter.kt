package com.example.player.presentation

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.player.R
import com.example.player.extensions.formatMillisecondsToTextMmSs
import com.example.player.model.Track
import kotlinx.android.synthetic.main.card_track.view.*

class MainAdapter(
    private val context: Context,
    private val onItemClickListener: OnItemClickListener
) : RecyclerView.Adapter<MainAdapter.MainViewHolder>() {

    private var listTrack = listOf<Track>()

    interface OnItemClickListener {
        fun onPlayListItemClicked(track: Track)
    }


    class MainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTitle: TextView = itemView.tv_card_title
        val tvArtists: TextView = itemView.tv_card_artist
        val tvDuration: TextView = itemView.tv_duration
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.card_track, parent, false)
        val holder = MainViewHolder(view)
        view.setOnClickListener {
            val adapterPosition = holder.adapterPosition
            if (adapterPosition != RecyclerView.NO_POSITION) {
                onItemClickListener.onPlayListItemClicked(listTrack[adapterPosition])
            }
        }
        return holder
    }

    override fun getItemCount() = listTrack.size

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.tvArtists.text = listTrack[position].artist
        holder.tvTitle.text = listTrack[position].title
        holder.tvDuration.text = listTrack[position].duration.formatMillisecondsToTextMmSs()
    }

    fun setData(list: List<Track>) {
        listTrack = list
        notifyDataSetChanged()
    }
}
