package com.example.player.presentation

import com.example.player.extensions.musicFiles

class MainPresenter(private val view: MainActivity) {

    fun getListTrack() {
        val list = view.applicationContext.musicFiles()
        view.setData(list)
        if (list.isNotEmpty()) {
            view.uiEnable()
        } else {
            view.uiDisable()
        }
    }
}
