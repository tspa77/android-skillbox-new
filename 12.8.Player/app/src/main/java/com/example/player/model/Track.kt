package com.example.player.model

import android.net.Uri

/**
 * Data class to hold music data
 */
data class Track(
    val artist: String,
    val title: String,
    val displayName: String,
    val duration: Int,
    val size: Int,
    val uri: Uri
)
